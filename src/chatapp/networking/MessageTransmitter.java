/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatapp.networking;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nicholas
 */
public class MessageTransmitter extends Thread {

    String message, hostname; //Message: o que enviar? Hostname: para onde enviar?
    int port;   //Port: para qual porta enviar?

    public MessageTransmitter() {   //Construtor

    }

    public MessageTransmitter(String message, String hostname, int port) { //Construtor overrided. Armazena as variáveis criadas dentro do construtor
        this.message = message;
        this.hostname = hostname;
        this.port = port;
    }

    @Override
    public void run() {
        try {
            Socket s = new Socket(hostname, port);    //Socket conecta com o socket do Servidor. O socket do servidor recebe um socket
            s.getOutputStream().write(message.getBytes());
            s.close();
        } catch (IOException ex) {
            Logger.getLogger(MessageTransmitter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
