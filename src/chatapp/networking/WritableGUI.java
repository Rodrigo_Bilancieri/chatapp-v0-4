/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatapp.networking;

/**
 *
 * @author Nicholas
 */
public interface WritableGUI {
    /*Interface vai servir pos, caso desejemos alterar o GUI, não será necessário alterar o MessageListeners*/
    void write(String s);
}