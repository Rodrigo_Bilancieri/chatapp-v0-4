package chatapp.networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nicholas
 */

public class MessageListener extends Thread {

    ServerSocket server;
    int port = 8877;
    WritableGUI gui;

    public MessageListener(WritableGUI gui, int port) {
        this.port = port;
        this.gui = gui;
        try {
            server = new ServerSocket(port);
        } catch (IOException ex) {
            Logger.getLogger(MessageListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public MessageListener() {
        try {
            server = new ServerSocket(port);
        } catch (IOException ex) {
            Logger.getLogger(MessageListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        Socket clientSocket; //Variável "clientSocket" do tipo "Socket". Cria uma conexão com uma porta
        try {
            while ((clientSocket = server.accept()) != null) { //O server aceita a conexão. A conexão é armazenada da variável "clientSocket". Loop fica aceitando conexão com o servidor. Quando é feita a conexão, ela é armazenada na variável "clientSocket". Se a conexão é "null", o loop acaba
                InputStream is = clientSocket.getInputStream(); //Pegar dados da conexão
                BufferedReader br = new BufferedReader(new InputStreamReader(is));  // Pega a InputStream da conexão estabelecida. BufferedReader transforma em stream
                String line = br.readLine(); //Recebe a linha da conversa
                if(line != null){ //Se a linha for diferente de null
                    gui.write(line); //passa para o método "write" contido na clase "MainScreen" a string da conversa
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(MessageListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
